const   express                 = require('express'),
        bp                      = require('body-parser'),
        flash                   = require('connect-flash'),
        mongoose                = require('mongoose'),
        expressSession          = require('express-session'),
        methodOverride          = require('method-override'),
        passport                =require('passport'),
        localStrategy           = require('passport-local'),
        passportLocalMongoose   = require('passport-local-mongoose');



// APP SETUP
const app = express();
app.use(bp.urlencoded({extended: true}));
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(methodOverride('_method'));
app.use(flash());

// DATABASE SETUP
mongoose.connect(`mongodb+srv://yelpcamp:${process.env.DB_PASS}@cluster0-vsw1v.mongodb.net/test?retryWrites=true`, { useNewUrlParser: true, useFindAndModify: false })
    .then(()=>{
        console.log('Database connected');
    })
    .catch(err =>{
        console.log('Error', err);
    })

// MODELS
const   User        = require('./models/user');

// ROUTES
const   campgroundRoutes    = require('./routes/campgrounds'),
        commentRoutes       = require('./routes/comments'),
        indexRoutes         = require('./routes/index');

// SEED DATABASE
const seedDB = require('./seed')
// seedDB();

// PASSPORT CONFIG
app.use(expressSession({
    secret: 'i need the hat rack back',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// PASS VARIABLES TO ALL ROUTES
app.use((req, res, next)=>{
    res.locals.currentUser = req.user;
    res.locals.error = req.flash('error');
    res.locals.success = req.flash('success');
    next();
})

// USE ROUTES
app.use(indexRoutes);
app.use('/campgrounds', campgroundRoutes);
app.use('/campgrounds/:id/comments', commentRoutes);

app.listen(process.env.PORT, ()=>{
    console.log('YelpCamp has started')
});