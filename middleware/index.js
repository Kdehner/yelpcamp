// CUSTOM MIDDLEWARE FOR APPLICATION
const Campground = require('../models/campground')
const Comment = require('../models/comment')
const middlewareObj = {}

middlewareObj.checkCampgroundOwnership = function(req, res, next){

    if(req.isAuthenticated()){
        Campground.findById(req.params.id)
        .then((campground)=>{
            if(campground.author.id.equals(req.user.id)){
                next();
            }
            else {
                req.flash('error', 'You do not have permission to do that');
                res.redirect('/campgrounds');
            }
        })
        .catch((err)=>{
            console.log('Error finding campground', err);
            req.flash('error', 'Something went wrong, Please try again');
            res.redirect('/campgrounds');
        })
    } else {
        req.flash('error', 'You must to be logged in to do that');
        res.redirect('/login');
    }    

}

middlewareObj.checkCommentOwnership = function(req, res, next){

    if(req.isAuthenticated()){
        Comment.findById(req.params.comment_id)
        .then((comment)=>{
            if(comment.author.id.equals(req.user.id)){
                next();
            }
            else {
                req.flash('error', 'You do not have permission to do that')
                res.redirect('back');
            }
        })
        .catch((err)=>{
            console.log('Error finding comment', err);
            req.flash('error', 'Something went wrong, Please try again');
            res.redirect('back');
        })
    } else {
        req.flash('error', 'You must be logged in to do that')
        res.redirect('/login');
    }
}

middlewareObj.isLoggedIn = function(req, res, next){
    
    if(req.isAuthenticated()){
        return next();
    }
    req.flash('error', 'You must to be logged in to do that')
    res.redirect('/login');
}

module.exports = middlewareObj