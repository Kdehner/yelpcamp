# YelpCamp

YelpCamp is a final project created for the [Web Developer BootCamp](https://www.udemy.com/share/100026AkATc1ZXRw==/) on Udemy by [Colt Steele](https://www.udemy.com/user/coltsteele/). 

# Goals

This project was used to learn to following tools and patterns.
    *HTML5/CSS3
    *JavaScript
    *NodeJS
    *ExpressJS
    *MongoDB
    *Authentication
    *RESTful Routing
    *Database Associations
    *Authorization

# Tech

This website depends on [MongoDB](https://www.mongodb.com) and [NodeJS](https://nodejs.org) to run.
