const   express     = require('express'),
        router      = express.Router();

// MODELS
const   Campground  = require('../models/campground');

// MIDDLEWARE
const middleware = require('../middleware')

// CAMPGROUNDS INDEX ROUTE
router.get('/', (req, res)=>{
    Campground.find({})
        .then((allCampgrounds)=>{
            res.render('campgrounds/index', {campgrounds: allCampgrounds});
        })
        .catch((err)=>{
            errorHandler()
        })
});

// CAMPGROUNDS NEW ROUTE
router.get('/new', middleware.isLoggedIn, (req, res)=>{
    res.render('campgrounds/new');
});

// CAMPGROUNDS CREATE ROUTE
router.post('/', middleware.isLoggedIn, (req, res)=>{
    //Get data from form
    const name = req.body.name;
    const image = req.body.image;
    const description = req.body.description;
    const author = {
        id: req.user._id,
        username: req.user.username
    }
    //Create new campground in DB
    Campground.create({
        name: name,
        image: image,
        description: description,
        author: author
    })
        .then((campground)=>{
            console.log('A new campground was added to the database', campground);
            req.flash('success', 'Your campground has been added');
            res.redirect('/campgrounds')
        })
        .catch((err)=>{
            errorHandler()
        })
});

// CAMPGROUNDS SHOW ROUTE
router.get('/:id', (req, res)=>{    
    Campground.findById(req.params.id).populate('comments').exec()
        .then((campground)=>{
            res.render('campgrounds/show', {campground: campground});
        })
        .catch((err)=>{
            errorHandler()

        })
});

// CAMPGROUNDS EDIT ROUTE
router.get('/:id/edit', middleware.checkCampgroundOwnership, (req, res)=>{
    Campground.findById(req.params.id)
        .then((campground)=>{
            res.render('campgrounds/edit', {campground:campground});
        })
        .catch((err)=>{
            errorHandler()
        })
});

// CAMPGROUNDS UPDATE ROUTE
router.put('/:id', middleware.checkCampgroundOwnership, (req, res)=>{
    Campground.findByIdAndUpdate(req.params.id, req.body.campground)
        .then((campground)=>{
            req.flash('success', 'Your campground has been updated');
            res.redirect(`/campgrounds/${req.params.id}`);
        })
        .catch((err)=>{
            errorHandler()
        })
});

// CAMPGROUNDS DELETE ROUTE
router.delete('/:id', middleware.checkCampgroundOwnership, (req, res)=>{
    Campground.findById(req.params.id)
        .then((campground)=>{
            campground.remove()
                .then(()=>{
                    req.flash('success', 'Your campground has been removed');
                    res.redirect('/campgrounds');
                })
                .catch((err)=>{
                    errorHandler()
                })
        })
        .catch((err)=>{
            errorHandler()
        })
})

function errorHandler(){
    console.log('ERROR PERFORMING ACTION', err);
    req.flash('error', 'Something went wrong, Please try again');
    res.redirect('/campgrounds');
}

module.exports = router;