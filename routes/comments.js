const   express     = require('express'),
        router      = express.Router({mergeParams: true});

// MODELS
const   Campground  = require('../models/campground'),
        Comment    = require('../models/comment');

// MIDDLEWARE
const middleware = require('../middleware')

// COMMENTS NEW ROUTE
router.get('/new', middleware.isLoggedIn, (req, res)=>{
    Campground.findById(req.params.id)
        .then((campground)=>{
            res.render('comments/new', {campground: campground});
        })
        .catch((err)=>{
            errorHandler();
        })
});

// COMMENTS CREATE ROUTE
router.post('/', middleware.isLoggedIn, (req, res)=>{
    Campground.findById(req.params.id)
        .then((campground)=>{
            Comment.create(req.body.comment)
                .then((comment)=>{

                    comment.author.id = req.user._id;
                    comment.author.username = req.user.username;
                    comment.save();

                    campground.comments.push(comment);
                    campground.save()
                    req.flash('success', 'Your review has been added');
                    res.redirect(`/campgrounds/${campground._id}`);
                })
                .catch((err)=>{
                    errorHandler();
                })
        })
        .catch((err)=>{
            errorHandler();
        })
});

// COMMENTS EDIT ROUTE
router.get('/:comment_id/edit', middleware.checkCommentOwnership, (req, res)=>{
    Campground.findById(req.params.id)
        .then((campground)=>{
            Comment.findById(req.params.comment_id)
                .then((comment)=>{
                    res.render('comments/edit', {comment: comment, campground: campground});
                })
                .catch((err)=>{
                    errorHandler();
                })
        })
        .catch((err)=>{
            errorHandler();
        })
});

// COMMENTS UPDATE ROUTE
router.put('/:comment_id', middleware.checkCommentOwnership, (req, res)=>{
    Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment)
        .then((comment)=>{
            req.flash('success', 'Your review has been updated');
            res.redirect(`/campgrounds/${req.params.id}`);
        })
        .catch((err)=>{
            errorHandler();
        })
});


// COMMENTS DELETE ROUTE
router.delete('/:comment_id', middleware.checkCommentOwnership, (req, res)=>{
    Comment.findByIdAndRemove(req.params.comment_id)
        .then(()=>{
            req.flash('success', 'Your review has been removed');
            res.redirect(`/campgrounds/${req.params.id}`);
        })
        .catch((err)=>{
            errorHandler();
        })
})

function errorHandler(){
    console.log('ERROR PERFORMING ACTION', err);
    req.flash('error', 'Something went wrong, Please try again');
    res.redirect('/campgrounds');
}

module.exports = router;