const   express     = require('express'),
        passport    = require('passport'),
        router      = express.Router();

// MODELS
const   User  = require('../models/user');

// LANDING ROUTE
router.get('/', (req, res)=>{
    res.render('landing');
});

// ===== REGISTER =====

// REGISTER SHOW ROUTE
router.get('/register', (req, res)=>{
    res.render('register');
});

// REGISTER CREATE ROUTE
router.post('/register', (req, res)=>{

    User.register(new User({username: req.body.username}), req.body.password)
        .then((user)=>{
            passport.authenticate('local')(req, res, function(){
                req.flash('success', `Sucessfully signed up! Welcome ${user.username}!`);
                res.redirect('/campgrounds');
            })
        })
        .catch((err)=>{
            console.log('ERROR PERFORMING ACTION', err);
            req.flash('error', err.message);
            res.redirect('/register')
        })
});

// ===== LOGIN =====

// LOGIN FORM
router.get('/login', (req, res)=>{
    res.render('login');
});

// LOGIN AUTHENTICATION
router.post('/login', passport.authenticate('local', {
    successRedirect: '/campgrounds',
    failureRedirect: '/login'
}), (req, res)=>{
});

// LOGOUT ROUTE
router.get('/logout', (req, res)=>{
    req.logOut();
    req.flash('success', 'Logged you out')
    res.redirect('/campgrounds');
})


module.exports = router;